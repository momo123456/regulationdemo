//
//  Array+RemoveDuplicate.swift
//  RegulationDemo
//
//  Created by 蔡宜臻 on 2021/6/30.
//

import Foundation

extension Array where Element: Hashable {
  func removingDuplicates() -> [Element] {
      var addedDict = [Element: Bool]()
      return filter {
        addedDict.updateValue(true, forKey: $0) == nil
      }
   }
   mutating func removeDuplicates() {
      self = self.removingDuplicates()
   }
}
