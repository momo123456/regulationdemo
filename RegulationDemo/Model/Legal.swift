//
//  LegalModel.swift
//  RegulationDemo
//
//  Created by 蔡宜臻 on 2021/6/28.
//

import Foundation

class Legal: NSObject {
    var ID: Int?
    var chapter: String?
    var text: String?
    
     init(ID: Int?, chapter: String?, text: String?) {
        self.ID = ID
        self.chapter = chapter
        self.text = text
    }
    
}
