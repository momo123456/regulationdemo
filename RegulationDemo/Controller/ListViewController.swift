//
//  ViewController.swift
//  RegulationDemo
//
//  Created by 蔡宜臻 on 2021/6/25.
//

import UIKit

private let MENU_TVCELL_ID = "MenuTableViewCell"

class ListViewController: UIViewController {
    
    @IBOutlet weak var listTableView: UITableView!
    @IBOutlet weak var menuTableView: UITableView!
    var legalList: [Legal] = [Legal]()
    var legalArray = [[Legal]]()
    var chapterList = [String]()
    var dbTool: DBTool = DBTool()
    var number = ["1","2","3","4"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.menuTableView.dataSource = self
        self.menuTableView.delegate = self
        // cell未超過顯示畫面時,不可滑動
        self.menuTableView.bounces = false
        self.listTableView.dataSource = self
        self.listTableView.delegate = self
        // 隱藏滾動條
        self.listTableView.showsVerticalScrollIndicator = false

        self.initParameter()

    }
    override func viewWillAppear(_ animated: Bool) {
        self.legalList = dbTool.queryData()
        dataCollation()
        self.listTableView.reloadData()
    }
    
    func dataCollation () {
        
        for legalList in self.legalList{
            if let chapter = legalList.chapter {
                chapterList.append(chapter)
            }
        }
        
        chapterList.removeDuplicates()
        print(chapterList)
        
        for i in 0...chapterList.count - 1 {
            var legalList2: [Legal] = [Legal]()
            
            for legalList in self.legalList {
                if chapterList[i] == legalList.chapter{
                    legalList2.append(legalList)
                }
            }
            self.legalArray.append(legalList2)
        }
        print(self.legalArray)
    }
    
    private func initParameter() {
        let menuTVCellID = UINib(nibName: MENU_TVCELL_ID, bundle: nil)
        self.menuTableView.register(menuTVCellID, forCellReuseIdentifier: MENU_TVCELL_ID)
    }
    
}
  //MARK: - UITableViewDelegate, UITableViewDataSource
extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.menuTableView {
            return number.count
        }
        return legalArray[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("tableView: \(tableView)")
        switch tableView {
        case listTableView:
            let cellIdentity = "ListCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentity) as! ListTableViewCell
            cell.setData(data: legalArray[indexPath.section][indexPath.row])
            return cell
        case menuTableView:

            let cell = tableView.dequeueReusableCell(withIdentifier: MENU_TVCELL_ID) as! MenuTableViewCell
            cell.numberLabel.text = number[indexPath.row]
            
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == self.menuTableView {
            
            return 1
        }
        return legalArray.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let chapterList = self.chapterList
        switch (section) {
        case 0:
            return chapterList[0]
        case 1:
            return chapterList[1]
        case 2:
            return chapterList[2]
        case 3:
            return chapterList[3]
        case 4:
            return chapterList[4]
        case 5:
            return chapterList[5]
        case 6:
            return chapterList[6]
        case 7:
            return chapterList[7]
        case 8:
            return chapterList[8]
        default:
            return nil
        }
    }
    
    //MARK: - 更改 Tableview Header In Section 字體大小
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let view = view as! UITableViewHeaderFooterView
        view.textLabel?.font = UIFont.systemFont(ofSize: 20)
        view.textLabel?.textColor = #colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.menuTableView{
            return 0
        }
        return 50
    }

}

