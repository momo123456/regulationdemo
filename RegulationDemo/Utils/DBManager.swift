//
//  DBManger.swift
//  RegulationDemo
//
//  Created by 蔡宜臻 on 2021/6/25.
//

import Foundation
import FMDB

class DBManager: NSObject {
    
    let DB_FILE_NAME = "demo_db.db"
    let fileManager = FileManager.default
    var filePath: String = ""
    var database: FMDatabase!
    
    //取得 documents folder 檔案路徑
    func getDBFromDoc() -> URL? {
        
        let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        /// guard 後專門描述我們希望成立的條件。當條件成立時，程式將離開 guard 搭配的 else { }，繼續往下執行我們希望條件成立時做的事。當 guard 的條件不成立時，將執行 else { } 的程式。
        /// else { } 的程式執行後，必須離開 guard 所在區塊，如此才不會繼續往下執行條件成立時要做的事。
        guard documentsUrl.count != 0 else {
            return nil
        }
        /// 取得檔案路徑
        let finalDatabaseURL = documentsUrl.first!.appendingPathComponent(DB_FILE_NAME)
        print("Doc: \(String(describing: finalDatabaseURL))")
        
        return finalDatabaseURL
    }
    

    /// 取得 .sqlite 連線
    func openConnection() -> Bool {
        //檢查DB是否存在，不存在就搬移Project內的DB
        copyDatabaseIfNeeded(DB_FILE_NAME)
        var isOpen: Bool = false
        
        self.database = FMDatabase(url: getDBFromDoc())
        
        if self.database != nil {
            if self.database.open() {
                isOpen = true
                print("ok")
            } else {
                print("Could not get the connection.")
            }
        }
        return isOpen
    }
    
    func copyDatabaseIfNeeded(_ database: String) {

        let fileManager = FileManager.default

        let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask)

        guard documentsUrl.count != 0 else {
            return
        }

        let finalDatabaseURL = documentsUrl.first!.appendingPathComponent("\(database)")

        if !( (try? finalDatabaseURL.checkResourceIsReachable()) ?? false) {
            print("DB does not exist in documents folder")

            let databaseInMainBundleURL = Bundle.main.resourceURL?.appendingPathComponent("\(database)")

            do {
                try fileManager.copyItem(atPath: (databaseInMainBundleURL?.path)!, toPath: finalDatabaseURL.path)
            } catch let error as NSError {
                print("Couldn't copy file to final location! Error:\(error.description)")
            }

        } else {
            print("Database file found at path: \(finalDatabaseURL.path)")
        }
    }
}
