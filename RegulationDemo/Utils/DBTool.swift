//
//  DBTool.swift
//  RegulationDemo
//
//  Created by 蔡宜臻 on 2021/6/28.
//

import Foundation
import FMDB

class DBTool: NSObject {
    
    var dbMsnager: DBManager = DBManager()

    func insertData(legal: Legal) {
        let inserSQL: String = "INSERT INTO Legal(ID, chapter, text) VALUES((SELECT IFNULL(MAX(ID), 0) + 1 FROM Legal), ?, ?)"
        if !self.dbMsnager.openConnection() {
            if ((self.dbMsnager.database?.executeUpdate(inserSQL, withArgumentsIn: [legal.ID ?? "", legal.chapter ?? "", legal.text ?? ""])) != nil) {
                print("Failed to insert initial data into the database.")
                print(dbMsnager.database.lastError(), dbMsnager.database.lastErrorMessage())
            }
            self.dbMsnager.database?.close()
        }
    }
    
    func updateData(legal: Legal) {
        let updateSQL: String = "UPDATE Legal SET chapter = ?, text = ? WHERE ID = ?"
        
        if self.dbMsnager.openConnection() {
            
            do {
                try self.dbMsnager.database?.executeUpdate(updateSQL, values: [legal.chapter ?? "", legal.text ?? "", legal.ID ?? 0])
            } catch {
                print(error.localizedDescription)
            }
            self.dbMsnager.database?.close()
        }
    }
    
    func queryData() -> [Legal] {
        var legalDatas: [Legal] = [Legal]()
        let querySQL: String = "SELECT * FROM Legal"
        if self.dbMsnager.openConnection() {
           
            do {
                let dataLists: FMResultSet = try self.dbMsnager.database.executeQuery(querySQL, values: nil)
                
                while dataLists.next() {
                    let legal: Legal = Legal(ID: Int(dataLists.int(forColumn: "ID")),
                                             chapter: dataLists.string(forColumn: "chapter"),
                                             text: dataLists.string(forColumn: "text"))
                    legalDatas.append(legal)
                }
            } catch {
                print(error.localizedDescription)
            }
            
        }
        return legalDatas
    }
    
    func deleteData(_ sID: Int) {
        let deleteSQL: String = "DELETE FROM Legal WHERE ID = ?"
        if self.dbMsnager.openConnection() {
            do {
                try self.dbMsnager.database?.executeUpdate(deleteSQL, values: [sID])
            } catch {
                print(error.localizedDescription)
            }
            self.dbMsnager.database?.close()
        }
    }
    
    func sortData(_ columnName: String, desc: String) -> [Legal] {
        let sortSQL: String = "SELECT * FROM Legal ORDER BY \(columnName) \(desc)"
        
        var sortSTDataList: [Legal] = [Legal]()
        if self.dbMsnager.openConnection() {
            do {
                let dataLists: FMResultSet = try dbMsnager.database.executeQuery(sortSQL, values: nil)
                
                while dataLists.next() {
                    let legal: Legal = Legal(ID: Int(dataLists.int(forColumn: "ID")), chapter: dataLists.string(forColumn: "chapter"), text: dataLists.string(forColumn: "text"))
                    sortSTDataList.append(legal)
                }
                
            } catch {
                print(error.localizedDescription)
            }
            
        }
        return sortSTDataList
    }
    
    
    
}
