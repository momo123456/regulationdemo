//
//  ListTableViewCell.swift
//  RegulationDemo
//
//  Created by 蔡宜臻 on 2021/6/28.
//

import UIKit

class ListTableViewCell: UITableViewCell {
    @IBOutlet weak var idLB: UILabel!
    @IBOutlet weak var textLB: UILabel!
    
    func setData(data: Legal) {
        guard let idLBText = data.ID else {
            return
        }
        self.idLB.text = "第 \(idLBText) 條"
        self.textLB.text = data.text
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
